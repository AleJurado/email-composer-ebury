import { Injectable } from '@angular/core';
import { Email } from '../data-model';
import { Router } from '@angular/router';

/**
 *  EmailDispatchService
 *
 *  A service to store all the email sended
 *
 *  
 **/
@Injectable({
  providedIn: 'root'
})
export class EmailDispatchService {

  constructor(private router : Router) { }

  emailSended = [];

  sendEmail(email : Email) {
    this.emailSended.push(email);

    this.router.navigate(['/success']);
  }
}
