import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})

/**
 *  FileUploadService
 *
 *  Service to store all the files uploaded
 *  
 **/
export class FileUploadService {

  constructor() { }

  files = [];

//Add file to current var
  addFile(file) {
    this.files.push(file);
  }

//Return an Observable with the 'files'
  getFiles(){
    return new Observable(observer => {
          observer.next(this.files);
    });
  }

//Return a determinated element from 'files'
  getImageSrc(index : number) {
    return this.files[index];   
  }

//Remove a file from 'files'
  removeFile(index : number) {
    this.files.splice(index, 1);
  }
}
