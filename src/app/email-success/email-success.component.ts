import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { EmailDispatchService } from '../services/email-dispatch.service';
import { Email } from '../data-model';

import { ThumbnailComponent } from '../shared/thumbnail/thumbnail.component';
import { FileUploadService } from '../services/file-upload.service';

@Component({
  selector: 'app-email-success',
  templateUrl: './email-success.component.html',
  styleUrls: ['./email-success.component.scss']
})
export class EmailSuccessComponent implements OnInit , AfterViewInit {

  constructor(private emailDispatcherService : EmailDispatchService, private fileUploadService : FileUploadService) { }
  currentEmail : Email;
  filesUpload;
  @ViewChild("inputFiles", {read: ElementRef}) inputFiles: ElementRef;
  
  ngOnInit() {
   console.log(this.emailDispatcherService.emailSended);
   this.currentEmail = this.emailDispatcherService.emailSended[0];
   this.fileUploadService.getFiles().subscribe(
    res => this.filesUpload = res
  );
  }

  ngAfterViewInit() {
    console.log(this.inputFiles); 
  }

}
