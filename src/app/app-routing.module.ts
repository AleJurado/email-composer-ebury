import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmailComposerComponent } from './email-composer/email-composer.component';
import { EmailSuccessComponent } from './email-success/email-success.component';

const routes: Routes = [
  { path: '', component: EmailComposerComponent } ,
  { path: 'success', component: EmailSuccessComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}