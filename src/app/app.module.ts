import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { EmailComposerComponent } from './email-composer/email-composer.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ThumbnailComponent } from './shared/thumbnail/thumbnail.component';

import { EmailSuccessComponent } from './email-success/email-success.component';
import { AppRoutingModule } from './app-routing.module';




@NgModule({
  declarations: [
    AppComponent,
    EmailComposerComponent,
    ThumbnailComponent,
    EmailSuccessComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
