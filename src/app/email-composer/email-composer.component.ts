import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ValidateEmails } from '../shared/email.validator';
import { ThumbnailComponent } from '../shared/thumbnail/thumbnail.component';
import { EmailDispatchService } from '../services/email-dispatch.service';
import { Email } from '../data-model';
import { FileUploadService } from '../services/file-upload.service';



@Component({
  selector: 'app-email-composer',
  templateUrl: './email-composer.component.html',
  styleUrls: ['./email-composer.component.scss']
})
export class EmailComposerComponent implements OnInit {
  filesUpload;
  emailForm:FormGroup;
  
  constructor(private fb: FormBuilder, private emailDispatchService : EmailDispatchService , private fileUploadService : FileUploadService) { }

  ngOnInit() {
    this.fileUploadService.getFiles().subscribe(
      res => this.filesUpload = res
    );
    this.emailForm = this.fb.group({
      'To': ['', [
        Validators.required,
        ValidateEmails
      ]],
      'CC': ['', [
        Validators.required,
        ValidateEmails
      ]],
      'BCC': ['', [
        Validators.required,
        ValidateEmails
      ]],
      'Subject': ['', [
        Validators.required,
        Validators.maxLength(100)
      ]],
      'Message': ['', [
        Validators.required
      ]],
      'Files' : ['']
    });
  }

  onSubmit() {
    let email : Email = this.emailForm.value;
    this.emailDispatchService.sendEmail(email);
  }

  

  onAddFile(e : Event, input : any) {
    let files = [].slice.call(e.target['files']);
     files.forEach((element , index)  => {
    this.fileUploadService.addFile(element);
     });
 
  }
    
}
