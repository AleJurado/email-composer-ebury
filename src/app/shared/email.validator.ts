import { AbstractControl } from '@angular/forms';

/**
 *  ValidateEmails
 *
 *  A validator to validate a undeterminated number of emails separated by comma
 *  
 **/

export function ValidateEmails(control: AbstractControl) {
  if (control.value !== null) {
   
    let emails : String[] = control.value.split(',');
   // let emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
    let emailPattern = "^([\\w-]+(?:\\.[\\w-]+)*)@((?:[\\w-]+\\.)*\\w[\\w-]{0,66})\\.([A-Za-z]{2,6}(?:\\.[A-Za-z]{2,6})?)$";
    let pass : boolean = true;
    emails.some(element => {
        
        if (!element.trim().match(emailPattern)) {
            pass = false;
            return true;
        }   
    });
    if (pass) {
        return null;
    } else {
        return { validEmails: false };
    }
    
  }
  return null;
}