import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { FileUploadService } from '../../services/file-upload.service';


/**
 *  ThumbnailComponent
 *
 *  This component is added dynamically to DOM
 *  It uses an 'index' to retrieve the file uploaded from FileUploadService 
 *  Also use a 'editMode' to allow or not the delete
 * 
 **/
@Component({
  selector: 'app-thumbnail',
  templateUrl: './thumbnail.component.html',
  styleUrls: ['./thumbnail.component.scss']
})
export class ThumbnailComponent implements OnInit {

  file;
  @Input() index: number;
  @Input() editMode: boolean;
  @ViewChild("img", {read: ElementRef}) img: ElementRef;

  constructor(private fileUploadService : FileUploadService) { }

  ngOnInit() {
    this.file = this.fileUploadService.getImageSrc(this.index);
    var reader  = new FileReader();
    reader.onloadend = () => {
      this.img.nativeElement.src = reader.result;
    }
    if ( this.file) {
      reader.readAsDataURL( this.file);
    }
  }

  removeFile(e : Event) {
    this.file = this.fileUploadService.removeFile(this.index);
  
  }

}
