export class Email {
    To : string = '';
    CC : string = '';
    BCC : string = '';
    Subject : string = '';
    Message : string = '';
    Files : string[];
  }
  
  